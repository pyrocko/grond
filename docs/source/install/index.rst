Installation
============

Grond is written in the Python programming language. To maintain Python
software installations, you can use a package manager, like pip, Conda, or the
system's native package manager - but you should not mix their use.

Grond is compatible with Python 3.7 and above. We provide pre-built packages
for pip, and instructions to install Grond within Conda environments and from
source.

If you want to use static displacement modelling (InSAR), additionally install
`Kite`_

Installation with pip (latest release package, from PyPI)
---------------------------------------------------------

Grond and all its dependencies can be installed by running

.. code-block:: bash

   pip3 install grond  # read below...

**but**, we recommend to make a conscious decision about how its main
dependency `Pyrocko`_ and especially Pyrocko's own dependencies are installed.
The `Pyrocko Installation Manual <https://pyrocko.org/docs/current/install/>`_
describes different installation schemes.

Installation under Conda (Grond latest release package, from PyPI)
--------------------------------------------------------------------

Pyrocko's pre-built Conda packages are available online, Grond can be
installed with Conda's pip into the *active* Conda environment.

.. code-block :: sh

    conda install -c pyrocko pyrocko
    pip3 install --no-deps grond

.. _manual-install:

Manual installation (from source)
---------------------------------

First install Grond's prerequisites. It depends on

* `Pyrocko`_ - required
* `Kite`_ - optional, needed for InSAR data modelling

Next, get Grond's source code using :program:`git clone`:

.. code-block :: sh

    git clone https://git.pyrocko.org/pyrocko/grond.git
    cd grond

Finally, decide if you want to install Grond system-wide, for a single
user or into a virtual or Conda environment (choose one):

- Installation in your home directory::

    # Assuming Pyrocko is installed system-wide or also in user home.
    pip3 install --no-deps --user .

- Installation into a Conda environment::

   # Assuming Conda environment is active and Pyrocko has been installed, for
   # example with `conda install -c pyrocko pyrocko`.
   pip3 install --no-deps .

- Installation into a virtual enviroment (venv)::

   # Assuming venv is active
   pip3 install .

- System-wide installation::

    # Assuming Pyrocko is already installed system-wide.
    sudo pip3 install --no-deps --break-system-packages .
    # Or, better, create a system wide venv with --system-site-packages and
    # include that in the system's bashrc configuration.

.. note ::

    The ``--no-deps`` option prevents :program:`pip` from installing Grond's
    requirements automatically. Pip's automatic dependency handling, only makes
    sense in pure pip environments.


Updating a manual (from source) installation
--------------------------------------------

For updating an existing manual installation of Grond, update the source code
using :program:`git pull`:

.. code-block :: sh

    cd grond  # change to the directory to where you cloned Grond initially
    git pull origin main

Then reinstall as described in section :ref:`manual-install` above.


.. _kite: https://pyrocko.org/kite/
.. _pyrocko: https://pyrocko.org/docs/current/install/
