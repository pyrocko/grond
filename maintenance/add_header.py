
import os

header = '''# https://pyrocko.org/grond - GPLv3
#
# The Grond Developers, 21st Century
'''


for top in ['src', 'test']:
    for root, dirs, files in os.walk(top):
        for file in files:
            if not file.endswith('.py'):
                continue

            path = os.path.join(root, file)

            with open(path, 'r') as f:
                text = f.read()

            if not text.startswith(header):
                with open(path, 'w') as f:
                    f.write(header+text)

                print('added header to %s' % path)
