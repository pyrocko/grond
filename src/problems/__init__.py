# https://pyrocko.org/grond - GPLv3
#
# The Grond Developers, 21st Century
'''
Definition of the objective function and source model parameter space.
'''

from .base import *  # noqa
from .cmt.problem import *  # noqa
from .locate.problem import *  # noqa
from .rectangular.problem import *  # noqa
from .double_dc.problem import *  # noqa
from .volume_point.problem import *  # noqa
from .singleforce.problem import *  # noqa
from .double_sf.problem import *  # noqa
from .simple_landslide.problem import *  # noqa
from .vlvd.problem import *  # noqa
